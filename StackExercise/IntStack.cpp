#include "IntStack.h"
#include "StackOverflowException.h"
#include "StackUnderflowException.h"

namespace StackExercise
{
	template<class T, size_t STACK_SIZE>
	GenericStack<T, STACK_SIZE>::GenericStack()
		: m_nextIndex(0)
	{}

	template <class T, size_t STACK_SIZE>
	void GenericStack<T, STACK_SIZE>::Push(T val)
	{
		if (m_nextIndex < STACK_SIZE)
		{
			m_impl[m_nextIndex] = val;
			++m_nextIndex;
		}
		else
		{
			throw StackOverflowException();
		}
	}

	template <class T, size_t STACK_SIZE>
	T GenericStack<T, STACK_SIZE>::Pop()
	{
		if (m_nextIndex > 0)
		{
			--m_nextIndex;
			return m_impl[m_nextIndex];
		}
		else
		{
			throw StackUnderflowException();
		}
	}
}
