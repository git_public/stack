#pragma once

namespace StackExercise
{
	template<class T, size_t STACK_SIZE>
	class GenericStack
	{
	public:
		GenericStack();
		void Push(T val);
		T Pop();

	private:
		T m_impl[STACK_SIZE];
		int m_nextIndex;
	};
}