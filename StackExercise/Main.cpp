#include <iostream>
#include "IntStack.h"
#include "IntStack.cpp"

void main()
{
	StackExercise::GenericStack<int, 100> s;
	s.Push(10);
	std::cout << s.Pop() << std::endl;
	getc(stdin);
	// ...
}